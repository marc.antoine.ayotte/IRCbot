import java.util.ArrayList;

public class Token {
    private ArrayList<String> parameterList;
    private String sender;

    public Token(ArrayList<String> pL, String s) {
        this.parameterList = pL;
        this.sender = s;
    }

    public Token() {

        this.parameterList = null;
        this.sender = null;
    }

    public String command(ArrayList<String> params, String sender) {
        /*Does nothing. Generic token when nothing needs to be done by bot.*/
        return null;
    }

    public ArrayList<String> getParameterList() {
        return this.parameterList;
    }

    public String getSender() {
        return this.sender;
    }

    public static class PingToken extends Token {
        public PingToken(ArrayList<String> pL, String s) {
            super(pL,s);
        }

        public String command(ArrayList<String> params, String sender) {
            return String.format("PONG %s\r\n", sender);
        }
    }

    public static class HelpToken extends Token {
        public HelpToken(ArrayList<String> pL, String s) {
            super(pL,s);
        }

        public String command(ArrayList<String> params, String sender) {
            return formatPRIVMSG(sender, "Hi! I'm a simple bot. Here's what I can do :")
                    + formatPRIVMSG(sender, "!add x1 .. xn : Returns the sum.")
                    + formatPRIVMSG(sender, "!sub x1 .. xn : Returns the difference.");
        }
    }

    public static class AddToken extends Token {
        public AddToken(ArrayList<String> pL, String s) {
            super(pL,s);
        }

        public String command(ArrayList<String> params, String sender) {
            double r = 0;
            for(String s : params) {
                r += tryParse(s);
            }
            return formatPRIVMSG(sender, "The result is " + r);
        }
    }

    public static class SubToken extends Token {
        public SubToken(ArrayList<String> pL, String s) {
            super(pL,s);
        }

        public String command(ArrayList<String> params, String sender) {
            double r = 2 * tryParse(params.get(0));
            for(String s : params) {
                r -= tryParse(s);
            }
            return formatPRIVMSG(sender, "The result is " + r);
        }
    }

    private static String formatPRIVMSG(String chat, String message) {
        return String.format("PRIVMSG %s :%s\n", chat, message);
    }

    private static double tryParse(String num) {
        double result = 0;
        try {
            result = Double.parseDouble(num);
        } catch (NumberFormatException e) {}
        return result;
    }
}
