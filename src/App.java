public class App {

    public static void main(String[] args) {
        Bot ircbot = new Bot(args[0], Integer.parseInt(args[1]), args[2], args[3], args[4]);
        ircbot.printOut();
        ircbot.joinChat(args[5]);
        ircbot.printOut();

        while(ircbot.receiveMessage()) {
            ircbot.printIn();
            ircbot.respond(ircbot.tokenize());
            ircbot.printOut();
        }
    }
}
