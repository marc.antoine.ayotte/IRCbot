import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;


public class Connection {
    private Socket socket;
    private PrintWriter clientOutput;
    private BufferedReader serverInput;
    private static Connection connection = null;

    private Connection(String host, int port){
        try {
            this.socket       = new Socket(host, port);
            this.clientOutput = new PrintWriter(socket.getOutputStream(), true);
            this.serverInput  = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch(Exception e) {
            System.out.println("Unable to establish connection. Exception occured :" + e.getMessage());
        }
    }

    public static Connection getConnection(String host, int port) {
        if(connection == null) {
            connection = new Connection(host, port);
        }

        return connection;
    }

    public void send(String s) {
        this.clientOutput.println(s);
    }

    public String receive() {
        String s = "";
        try {
            s = this.serverInput.readLine();
        } catch (Exception e) {
            System.out.println("Problem retrieving server input.");
        }

        return s;
    }
}
