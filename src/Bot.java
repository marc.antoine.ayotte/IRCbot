import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;

public class Bot {

    private Connection connection;
    private String bufferOut;
    private String bufferIn;

    public Bot(String host, int port, String nick, String user, String real) {
        this.bufferIn = "";
        this.bufferOut = "";
        this.connection = Connection.getConnection(host, port);
        this.joinServer(nick, user, real);
    }

    public Token tokenize() {
        String[] inputs = bufferIn.split(" ");
        Token token = new Token();
        ArrayList<String> parameterList = new ArrayList<>();
        String sender = inputs.length >= 3 ? inputs[2] : null;

        if (inputs[0].equals("PING")) {
            sender = inputs[1];
            token = new Token.PingToken(parameterList, sender);
        } else if (inputs.length >= 4) {
            switch(inputs[3]) {
                case ":!add":  token = new Token.AddToken(parameterList, sender);
                    break;
                case ":!sub":  token = new Token.SubToken(parameterList, sender);
                    break;
                case ":!help": token = new Token.HelpToken(parameterList, sender);
                    break;
                default:       token = new Token(parameterList, sender);
            }

            for(int i = 4; i < inputs.length; ++i) {
                parameterList.add(inputs[i]);
            }

        }


        return token;
    }

    /*Updates buffer and sends answer*/
    public void respond(Token token) {
        this.bufferOut = token.command(token.getParameterList(), token.getSender());
        if(this.bufferOut != null)
        this.connection.send(this.bufferOut);
    }

    public void joinChat(String chatName) {
        this.bufferOut = String.format("JOIN %s\r\n", chatName);
        this.connection.send(bufferOut);
    }

    //Wait for server message and update bufferIn
    //Returns true if a message is received.
    public boolean receiveMessage() {
        this.bufferIn = this.connection.receive();
        return bufferIn != null;
    }

    //The bot's input (Server's output) if there is any
    public void printIn() {
        if(this.bufferIn != null && !this.bufferIn.equals(""))
            System.out.println("Server->" + this.bufferIn);
    }

    //The bot's output(Server's input) if there is any
    public void printOut() {
        if(this.bufferOut != null && !this.bufferOut.equals(""))
            System.out.println("Bot->" + this.bufferOut);
    }

    //The bot can connect to only one server.
    private void joinServer(String nick, String user, String real) {
        this.bufferOut = String.format("NICK %s\r\nUSER %s 0 * :%s\r\n", nick, user, real);
        this.connection.send(bufferOut);
    }


}
